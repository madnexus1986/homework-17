// Homework 17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <math.h>

class MyClass
{
private:
    int MyNumber = 10;
    char MyText[255] = "Hello!";
    
public:
    void ShowAll()
    {
        std::cout << MyText << "\n";
        std::cout << MyNumber << "\n";
    }
};

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void ShowVectorComponents()
    {
        std::cout  << x << ' ' << y << ' ' << z << '\n';
    }
    double VectorLength(double _x1, double _y1, double _z1)
    {
        length = sqrt(pow(_x1, 2)+ pow(_y1, 2)+ pow(_z1, 2));
              return length;
    }
private:
    double x;
    double y;
    double z;
    double length;
};

int main()
{
    MyClass MyData;
    MyData.ShowAll();
    Vector vec;
    vec.ShowVectorComponents();
    std::cout << "Vector length = " << vec.VectorLength(10, 5, 5);
}


